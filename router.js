const express = require('express');
var bodyParser = require('body-parser');
var fs = require('fs');
var https = require('https');
var http = require('http');

const app = express()

// create application/json parser
var jsonParser = bodyParser.json()


var locations = [];

app.use(express.static(__dirname + '/public'));

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});



app.get('/getStuff', function(req, res){
    res.send(locations);
})


// POST /api/users gets JSON bodies
app.post('/sendStuff', jsonParser, function (req, res) {
  if (!req.body) return res.sendStatus(400)
  // create user in req.body
  console.log(req.body);
  locations.push(req.body);
  res.send(locations);
})


var httpServer = http.createServer(app);


httpServer.listen(8080);
